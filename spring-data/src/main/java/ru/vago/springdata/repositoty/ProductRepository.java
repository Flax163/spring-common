package ru.vago.springdata.repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.vago.springdata.data.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByDescription(String description);

    @Query(value = "from Product p join fetch p.items i")
    List<Product> findAllProductsFetchItems();
}