package ru.vago.springdata.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.vago.springdata.data.Product;
import ru.vago.springdata.dto.ProductDto;
import ru.vago.springdata.service.ProductService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public List<ProductDto> loadAllProducts() {
        return productService.loadProducts();
    }

    @GetMapping("/{id}")
    public ProductDto loadProductById(@PathVariable Long id) {
        return productService.loadProductById(id);
    }

    @GetMapping("/descriptions/{description}")
    public List<ProductDto> loadProductByDescriptions(@PathVariable String description) {
        return productService.loadProductByDescription(description);
    }

    @PostMapping
    public Long createProduct(@RequestBody ProductDto productDto) {
        return productService.createProduct(productDto);
    }
}
