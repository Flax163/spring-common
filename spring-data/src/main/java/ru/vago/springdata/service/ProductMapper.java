package ru.vago.springdata.service;

import org.mapstruct.Mapper;
import ru.vago.springdata.data.Item;
import ru.vago.springdata.data.Product;
import ru.vago.springdata.dto.ItemDto;
import ru.vago.springdata.dto.ProductDto;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    Product toProduct(ProductDto productDto);

    ProductDto toProductDto(Product product);

    Item toItem(ItemDto itemDto);

    ItemDto toItemDto(Item item);
}
