package ru.vago.springdata.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vago.springdata.dto.ProductDto;
import ru.vago.springdata.repositoty.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Transactional(readOnly = true)
    public ProductDto loadProductById(Long id) {
        return productRepository.findById(id)
                .map(productMapper::toProductDto)
                .orElseThrow(IllegalStateException::new);
    }

    @Transactional(readOnly = true)
    public List<ProductDto> loadProductByDescription(String description) {
        return productRepository.findByDescription(description).stream()
                .map(productMapper::toProductDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ProductDto> loadProducts() {
        return productRepository.findAll().stream()
                .map(productMapper::toProductDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public Long createProduct(ProductDto productDto) {
        var product = productMapper.toProduct(productDto);
        productRepository.save(product);
        return product.getId();
    }
}