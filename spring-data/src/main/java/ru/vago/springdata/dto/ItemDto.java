package ru.vago.springdata.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ItemDto {
    Long id;
    String name;
}