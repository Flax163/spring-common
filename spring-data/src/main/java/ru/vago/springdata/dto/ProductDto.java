package ru.vago.springdata.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ProductDto {
    String id;
    String name;
    String description;
    List<ItemDto> items;
}