--liquibase formatted sql

--changeset vgoncharov:create init

CREATE TABLE PRODUCTS
(
    ID          bigserial,
    NAME        varchar(50) NOT NULL,
    DESCRIPTION varchar(200),
    CONSTRAINT PRODUCTS_PK PRIMARY KEY (ID)
);

CREATE TABLE ITEMS
(
    ID          bigserial,
    NAME        varchar(50) NOT NULL,
    CONSTRAINT  ITEMS_PK PRIMARY KEY (ID)
);

CREATE TABLE PRODUCTS_ITEMS
(
    PRODUCT_ID int8 NOT NULL,
    ITEMS_ID   int8 NOT NULL,
    CONSTRAINT FK_PRODUCTS_ITEMS_PRODUCT_ID FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS (ID),
    CONSTRAINT FK_PRODUCTS_ITEMS_ITEMS_ID FOREIGN KEY (ITEMS_ID) REFERENCES ITEMS (ID)
);