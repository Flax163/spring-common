package ru.vago.redis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vago.redis.service.IncService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class IncController {
    private final IncService incService;

    @PostMapping("/inc")
    public void increment() {
        incService.increment();
    }

    @GetMapping("/value")
    public int getValue() {
        return incService.getValue();
    }
}
