package ru.vago.redis.parameters;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.time.Duration;

/**
 * Параметры для конфигурации редиса
 */
@Data
@Validated
@Component
@ConfigurationProperties("redis")
public class RedisProperties {
    private static final int MAX_TOTAL_DEFAULT = 1000;
    private static final int MAX_IDLE_DEFAULT = 10;
    private static final int MIN_IDLE_DEFAULT = 1;
    private static final Duration MAX_WAIT_DEFAULT = Duration.ofSeconds(30);
    private static final int TIMEOUT_DEFAULT = 2000;

    /**
     * путь до хоста
     */
    @NotBlank
    private String host;
    /**
     * Порт
     */
    private int port;
    /**
     * Пользователь
     */
    private String user;
    /**
     * Пароль
     */
    private String password;
    /**
     * Максимальное число соединений
     */
    private Integer maxTotal = MAX_TOTAL_DEFAULT;
    /**
     * Максимальное количество свободных соединений
     */
    private Integer maxIdle = MAX_IDLE_DEFAULT;
    /**
     * Минимальное количество свободных соединений
     */
    private Integer minIdle = MIN_IDLE_DEFAULT;
    /**
     * Максимальное время ожидания
     */
    private Duration maxWait = MAX_WAIT_DEFAULT;
    /**
     * Время до таймаута в милисекундах
     */
    private Integer timeout = TIMEOUT_DEFAULT;
}