package ru.vago.redis.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import ru.vago.redis.parameters.RedisProperties;

@Component
@RequiredArgsConstructor
public class JedisConfiguration {
    public static final String JEDIS_POOL_QUALIFIER = "jedisPool";
    private final RedisProperties redisProperties;

    @Bean
    @Qualifier(JEDIS_POOL_QUALIFIER)
    public JedisPool jedisPool() {
        var config = new JedisPoolConfig();
        config.setMaxTotal(redisProperties.getMaxTotal());
        config.setMaxIdle(redisProperties.getMaxIdle());
        config.setMinIdle(redisProperties.getMinIdle());
        config.setMaxWait(redisProperties.getMaxWait());
        return new JedisPool(config, redisProperties.getHost(), redisProperties.getPort(), redisProperties.getTimeout(),
                trimToNull(redisProperties.getUser()), trimToNull(redisProperties.getPassword()));
    }

    private String trimToNull(String content) {
        if (content == null) {
            return null;
        } else if (content.isBlank()) {
            return null;
        } else {
            return content;
        }
    }
}