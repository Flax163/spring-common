package ru.vago.redis.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisPool;

import static ru.vago.redis.config.JedisConfiguration.JEDIS_POOL_QUALIFIER;

@Service
public class IncService {
    private static final String TRUST_GATE_TOKEN_KEY = "REDIS_EXAMPLE:VALUE";
    private final JedisPool jedisPool;

    public IncService(@Qualifier(JEDIS_POOL_QUALIFIER) JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void increment() {
        try (var jedis = jedisPool.getResource()) {
            jedis.incr(TRUST_GATE_TOKEN_KEY);
        }
    }

    public int getValue() {
        try (var jedis = jedisPool.getResource()) {
            var value = jedis.get(TRUST_GATE_TOKEN_KEY);
            if (value == null) {
                return 0;
            }
            return Integer.parseInt(value);
        }
    }
}